#Enonic XP Partnamespace Library
Makes it easy to add namespaces to parts in Html DOM and Javascript Config

#Namespacing in JavaScript

Global variables should be reserved for objects that have system-wide relevance and they should be named to avoid ambiguity and minimize the risk of naming collisions. In practice this means you should avoid creating global objects unless they are absolutely necessary.

#What this lib is for
In Enonic XP, parts can include their own client scripts by leveraging [page contributions](http://xp.readthedocs.org/en/stable/developer/site/contributions.html). Often the client script 
needs some config that is available in the part. One way of doing this is creating a namespaced javascript config object on the window, and accessing this in the client script. This library
uses the app namespace to create names that avoid ambiguity and minimize the risk of naming collisions. It can be used to minimize the coupling in your app by using safe global variables instead
on depending on hooks and relationships in the Html DOM. In the examples below I will use this app structure where one part has a controller and a view and a corresponding client script in the assets folder.
See documentation on standard Enonic XP app [project structure](http://xp.readthedocs.org/en/stable/developer/projects/structure.html) if confused.

    site
        assets
            parts
                mypart
                    mypartClient.js
        parts
            mypart
                mypart.js
                mypart.html


#Javascript controller (mypart.js)

##Require dependency
    var partnamespace = require('/lib/openxp/partnamespace');

##Add all or specific namespaces to model
###Add all namespaces
    model.partnamespace = partnamespace.getNs();
    
    Example return value:
    {    
        app: 'com_yourcompany_yourapp',
        part: 'com_yourcompany_yourapp_yourpart',
        local: 'com_yourcompany_yourapp_yourpart_yourregion_0'
    }
###Add individual namespaces
    partnamespace.getAppNs();
    partnamespace.getPartNs();
    partnamespace.getLocalNs();

##Add namespacing to client script
Will add **data-partnamespacescript='com_yourcompany_yourapp_yourpart'** to script element, which can be leveraged
in client script to link it to its part, when using script as a [page contribution](http://xp.readthedocs.org/en/stable/developer/site/contributions.html)
    
    return {
        body: [body],
        pageContributions: {
            headEnd: [
                partnamespace.getNsScript('parts/tags/tagsClient.js')
            ]
        }
    };

#View (mypart.html)    
When adding **model.partnamespace = partnamespace.getNs()** to the model, you can use it in your view to namespace entire part and create scoped javascript config
    
    <div data-th-attr="data-partnamespace=${partnamespace.part}">
        <script data-th-inline="text">
            /*<![CDATA[*/
            window.[[${partnamespace.part}]] = {
                appNs: '[[${partnamespace.app}]]',
                partNs: '[[${partnamespace.part}]]',
                localNs: '[[${partnamespace.local}]]',
                
                //Examples of useful config that can be used in the client javascript belonging to this part
                componentUrl: '[[${componentUrl}]]',
                updateSomethingServiceUrl: '[[${serviceUrl}]]',
                
                //Example of using app-namespace for variable used across multiple parts
                sessionStorageKey: '[[${partnamespace.app}]]_sharedSessionStorageKey'
            };
            /*]]>*/
        </script>
    </div>

#Client javascript (mypartClient.js)
Since the script element and the part has the same value in the data-partnamespace attribute, we can link the two together by using some javascript magic, and then being 
able to access all the part config inside the client script.

NB! Example uses some easily rewritable ES6 syntax and dataset attribute which has limited IE 10 support

    let mypartClient = (function() {
        let init = function() {
            //Add inline script config from part to config object
            Object.assign(config, window[config.partnamespace]);
            config.partElementSelector = '*[data-partnamespace="'+config.partnamespace+'"]';
            config.partElement = document.querySelector(config.partElementSelector);
        };
        
        
        //Example of adding a click eventlistener to the part and toggle a class
        let registerPartClickEvent = function() {
            config.partElement.addEventListener('click', handlePartClick, false);
        };
        
        var handlePartClick = function(event) {
            if (event.target !== event.currentTarget) {
                let clickedItemId = event.target.id;
                event.target.classList.toggle('selected');
            }
            event.stopPropagation();
        };
        
        return {
            init: init
        };
    })()

    document.addEventListener('DOMContentLoaded', function () {
        mypartClient.init();
    });
    
    /**
     * Current Script Namespace
     *
     * Get the dir path to the currently executing script file
     * which is always the last one in the scripts array with
     * an [src] attr
     */
    var config = {};
    (function () {
        var scripts = document.querySelectorAll( 'script[src]' );
        var currentScript = scripts[ scripts.length - 1 ];
        config.partnamespace = currentScript.dataset.partnamespacescript;
    })();
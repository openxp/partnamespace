var libs = {
    portal: require('/lib/xp/portal'),
    content: require('/lib/xp/content')
};
/**
 * Returns namespaces for app, part and local
 * */
exports.getNs = function () {
    return {
        app: exports.getAppNs(),
        part: exports.getPartNs(),
        local: exports.getLocalNs()
    }
};

/**
 * Returns app namespace
 * Example: com_yourcompany_yourapp
 * */
exports.getAppNs = function () {
    if (!app || !app.name){
        return '';
    }
    return removeBadChar(app.name);
};

/**
 * Returns part namespace
 * Example: com_yourcompany_yourapp_yourpart
 * */
exports.getPartNs = function () {
    var component = libs.portal.getComponent();
    if (!component) {return null;}
    return removeBadChar(component.descriptor);
};

/**
 * Returns local namespace
 * Example: com_yourcompany_yourapp_yourpart_yourregion_0
 * */
exports.getLocalNs = function () {
    var component = libs.portal.getComponent();
    if (!component) {return null;}
    return removeBadChar(component.descriptor + "_" + component.path);
};

/**
 * Returns script with data-partnamespacescript='com_yourcompany_yourapp_yourpart' attribute
 * You can use this data attribute to access the correct part in the DOM using this code in client script, and
 * such linking the part and the part client script together. See example in Readme.md for details
 *
 * @param scriptPath
 * Example: partnamespace.getNsScript('parts/myPart/myPartClient.js')
 * Will look for script in site/assets/ folder
 * <script data-partnamespacescript="openxp_starter_skyscraper_tags" src="[path]" type="text/javascript"></script>
 * */
exports.getNsScript = function (scriptPath) {
    var partNs = exports.getPartNs();
    var appNs = exports.getAppNs();
    if (partNs){
        return "<script src='" + libs.portal.assetUrl({path: scriptPath}) + "' " +
            "type='text/javascript' " +
            "data-partnamespacescript='" + partNs + "'></script>";
    }else if (appNs){
        return "<script src='" + libs.portal.assetUrl({path: scriptPath}) + "' " +
            "type='text/javascript' " +
            "data-partnamespacescript='" + appNs + "'></script>";
    }else{
        return "<script src='" + libs.portal.assetUrl({path: scriptPath}) + "' type='text/javascript'></script>";
    }

};

var removeBadChar = function (ns) {
    if (ns===undefined){
        throw "App namespace must be defined in partnamespace lib";
    }
    return ns.replace(/[^a-z0-9_]/ig, '_').toLowerCase();
};
